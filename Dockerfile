ARG FLUTTER_VERSION=3.22.0
FROM ghcr.io/cirruslabs/flutter:${FLUTTER_VERSION}
RUN apt update && \
    apt install -y --no-install-recommends \
    cmake \
    ninja-build \
    clang \
    build-essential \
    pkg-config \
    libgtk-3-dev \
    liblzma-dev \
    lcov
RUN flutter config --enable-linux-desktop
RUN wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt install -y ./google-chrome-stable_current_amd64.deb && \
    rm ./google-chrome-stable_current_amd64.deb
RUN flutter config --enable-web
RUN gem install fastlane
